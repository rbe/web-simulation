var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.json({
        'status': 'OK',
        'code': 200,
        'message': 'request success!'
    });
});

module.exports = router;
