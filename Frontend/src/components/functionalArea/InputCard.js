import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';
require('es6-promise').polyfill();

import { Card, Button, Row, Col, Input, InputNumber, Form } from 'antd';

const FormItem = Form.Item;

export default class InputCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            symbol: 5,
            ratio: 20
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        const value = this.state;
        const props = this.props;

        fetch('http://localhost:3001/api/projects/qam', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': "application/json",
                'Accept': "application/json"
            },
            body: JSON.stringify({data: value})
        })
            .then(res => res.json())
            .then(data => () => {
                props.callbackParent({
                    images: data.data.images
                });
            })
            .catch(err => console.log(err));
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <Card title="Input" className="card">
                <Form layout="inline" onSubmit={this.handleSubmit}>
                    <FormItem
                        label="十进制符号数"
                        labelCol={{span: 10}}
                        wrapperCol={{span: 14}}
                    >
                        <Input
                            name="symbol"
                            value={this.state.symbol}
                            onChange={this.handleInputChange}
                        />
                    </FormItem>
                    <FormItem
                        label="信号量噪比"
                        labelCol={{span: 10}}
                        wrapperCol={{span: 14}}
                    >
                        <Input
                            name="ratio"
                            value={this.state.ratio}
                            onChange={this.handleInputChange}
                        />
                    </FormItem>
                    <FormItem>
                        <Button type="primary" size="large" htmlType="submit">
                        提交
                        </Button>
                    </FormItem>
                    <FormItem>
                        <Button type="danger" size="large" ghost>
                        重置
                        </Button>
                    </FormItem>
                </Form>
            </Card>
        );
    }
}
