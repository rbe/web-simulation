import React, { Component } from 'react';

import { Card } from 'antd';

export default class Simulation extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { images } = this.props;

        return (
            <Card title="Simulation" className="card card-large">
                images.map((item) => {
                    return (
                        <img
                            className="simulation-area"
                            src={item}
                        />
                    )
                })
            </Card>
        );
    }
}
