import React, { Component } from 'react';

import { Card } from 'antd';

export default class Simulation extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Card title="Code" className="card card-large">
                <div className="code-area">This Place Will Show You the Code...</div>
            </Card>
        );
    }
}
