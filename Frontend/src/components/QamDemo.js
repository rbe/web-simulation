import React, { Component } from 'react';

import { Card, Button, Row, Col, Input } from 'antd';

import InputCard from './functionalArea/InputCard';
import AnalysisCard from './functionalArea/AnalysisCard';
import SimulationCard from './functionalArea/SimulationCard';
import CodeCard from './functionalArea/CodeCard';

import '../stylesheets/QamDemo.less';

export default class Configuration extends Component {
    constructor(props) {
        super(props);

        this.state={
            images: []
        }

        this.changeSimulationImage = this.changeSimulationImage.bind(this);
    }

    changeSimulationImage(images) {
        this.setState({
            images: images
        });
    }

    render() {
        const { images } = this.state;

        return (
            <div className="QamDemo-container">
                <Row
                    type="flex"
                    justify="space-around"
                    gutter={16}
                    className="configuration-container"
                >
                    <Col span={16} className="gutter-row">
                        <InputCard
                            callbackParent={(images) => this.changeSimulationImage(images)}
                        />
                    </Col>
                    <Col span={8} className="gutter-row">
                        <AnalysisCard />
                    </Col>
                </Row>
                <Row
                    type="flex"
                    justify="space-around"
                    gutter={16}
                    className="configuration-container"
                >
                    <Col span={16} className="gutter-row">
                        <SimulationCard
                            imagePaths={images}
                        />
                    </Col>
                    <Col span={8} className="gutter-row">
                        <CodeCard />
                    </Col>
                </Row>
            </div>);
    }
}
