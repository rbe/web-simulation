import fetch from 'isomorphic-fetch';

export const request = (url, options) => {
    const headers = {
        'Content-Type': "application/json",
        'Accept': "application/json"
    };
    const promise = fetch(url, Object.assign({
        headers,
        mode: 'cors'
    }, options));
    return promise;
};
