import React, { Component } from 'react';
import { HashRouter as Router, Route, Link } from 'react-router-dom';
import createBrowserHistory from 'history/es/createBrowserHistory';

import { Menu, Icon, Row, Col } from 'antd';
const MenuItem = Menu.Item;
const SubMenu = Menu.SubMenu;

import './stylesheets/app.less';

import Welcome from './components/Welcome';
import QamDemo from './components/QamDemo';
import OfdmDemo from './components/OfdmDemo';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router history={ createBrowserHistory }>
                <div className="app-container">
                    <div className='banner-area'>
                        <p>Web-Simulation</p>
                    </div>
                    <Row gutter={16}>
                        <Col span={4}>
                            <Menu
                                defaultOpenKeys={['sub1']}
                                mode='inline'
                                style={{
                                    paddingBottom: 1024,
                                    overflow: 'hidden'
                                    }}
                            >
                                <SubMenu
                                    title={<span><Icon type="appstore" /><span>项目案例</span></span>}
                                    key="sub1"
                                >
                                    <MenuItem key="1"><Link to="/project/qam">QAM(16bit)</Link></MenuItem>
                                    <MenuItem key="2"><Link to="/project/ofdm">OFDM</Link></MenuItem>
                                </SubMenu>
                                <MenuItem key="3"><span><Icon type="setting" /></span>在线交互</MenuItem>
                            </Menu>
                        </Col>
                        <Col span={20}>
                            <Route exact path="/" component={Welcome}></Route>
                            <Route path="/project/qam" component={QamDemo}></Route>
                            <Route path="/project/ofdm" component={OfdmDemo}></Route>
                        </Col>
                    </Row>
                </div>
            </Router>
        );
    }
}

export default App;
