const express = require('express');
const path = require('path');
const webpack = require('webpack');

const app = express();
const router = express.Router();

app.use(express.static('dist'));

router.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.use(router);

app.listen(3000, () => {
    console.log('Listening on Port 3000...');
});
